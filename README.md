# CONFIGURAÇÃO DE AMBIENTE - FRONTEND - (UBUNTU) #

Realizar update na maquina e reiniciar o navegador após a atualização - Software updater

**Instalar Chrome debian x64 - download **
	*Instalação do pacote realizada via navegador/ ubunto software center	

**Instalar Sublime:**
	
	sudo add-apt-repository ppa:webupd8team/sublime-text-3
	sudo apt-get update
	sudo apt-get install sublime-text-installer

**Instalar Package Control Sublime**

**Instalar Git:**

	sudo apt-get install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev gitk

**Clonar Projeto:**

	*Caso o usuário não possua o projeto, informar o usuário do bitbucket a um dos administradores do projeto para a sua inclusão.
	*alterar usuario e senha da url abaixo	

	git clone https://usuario:senha@bitbucket.org/sptransfront/sptrans.git

**Instalar Node/NPM:**

	curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
	sudo apt-get install -y nodejs

**Instalar Grunt:**

	sudo npm install -g grunt-cli

**Instalar Dependencias do NPM**
	*Estar na pasta do projeto sptrans
	
	npm install

**Instalar Ruby:**

	sudo apt-get install ruby-full

**Instalar Compass:**

	- Instalar compass

	sudo gem update --system
	sudo gem install compass

**Instalar Xampp:**

	- Realizar download do executavel:

	wget http://tenet.dl.sourceforge.net/project/xampp/XAMPP%20Linux/5.6.19/xampp-linux-x64-5.6.19-0-installer.run -O xampp-installer.run	
	
	- Instalar xamp	

	chmod +x xampp-installer.run
	sudo ./xampp-installer.run

	- Ao abrir o xamp, abrir o arquivo httpd.conf no sublime:	

	subl /opt/lampp/etc/httpd.conf 

	- Ao abrir o arquivo no sublime adicionar os seguintes trechos de código no arquivo httpd.conf:
	* Alterar usuario-x pelo nome do usuário 
	
		<Directory "/sptrans/">
		Options Indexes FollowSymLinks ExecCGI Includes
		AllowOverride All
		Require all granted
		</Directory>

		Alias /sptrans "/home/usuario-x/Projects/sptrans/dist/boffice/"

	
	- Comando para abrir o arquivo httpd.conf no sublime	
		
		subl /opt/lampp/etc/extra/httpd-xampp.conf

	- Ao abrir o arquivo no sublime adicionar os seguintes trechos de código no arquivo httpd.conf:
	* Alterar usuario-x pelo nome do usuário 
		
		Alias /sptrans "/home/usuario-x/Projects/sptrans/dist/boffice/"

		<Directory "/home/usuario-x/Projects/sptrans/dist/boffice/">
		AllowOverride AuthConfig Limit
		Require local
		ErrorDocument 403 /error/XAMPP_FORBIDDEN.html.var
		</Directory>

	- Restartar o xampp após a alteração dos arquivos arquivos

	sudo /opt/lampp/lampp restart
