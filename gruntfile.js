module.exports = function (grunt) {
    'use strict';
    // Project configuration.
    grunt.initConfig({
        path: {
            css: 'dist/boffice/estaticos/css/',
            fonts: 'dist/boffice/estaticos/fontes/',
            img: 'dist/boffice/estaticos/imagens/',
            app: 'dist/boffice/app/',
            
            sass: 'source/boffice/sass/',
            fontssrc: 'source/boffice/fontes/',
            imgsrc: 'source/boffice/imagens/',
            appsrc: 'source/boffice/app/'
        },
        //Watch Task
        watch: {
            options: {
                livereload: true
            },
            sass: {
                files: '<%= path.sass %>**/*.scss',
                tasks: ['compass:compile']
            },
            html: {
                files: ['<%= path.appsrc %>**/*.html','source/boffice/index.html' ],
                tasks: ['copy:index', 'htmlmin:allhtml']
            },
            js: {
                files: '<%= path.appsrc %>**/**/*.js',
                tasks: ['copy:alljs', 'uglify:myjs']
            },
            img: {
                files: '<%= path.imgsrc %>**/*.{jpg,gif,png}',
                tasks: ['imagemin:allimg']
            },
            fonts: {
                files: '<%= path.fontssrc %>**/*.{ttf,svg,woff,woof2,eot}',
                tasks: ['copy:allfonts']
            }
        },
        //Clean Task
        clean: {
            build: ['dist/boffice/**']
        },
        //Copy Task
        copy: {
            alljs: {
                expand: true,
                cwd: '<%= path.appsrc %>',
                src: '**/**/*.js',
                dest: '<%= path.app %>'
            },
            allcss: {
                expand: true,
                cwd: '<%= path.sass %>',
                src: '**/*.css',
                dest: '<%= path.css %>'
            },
            allfonts: {
                expand: true,
                cwd: '<%= path.fontssrc %>',
                src: '**/*.*',
                dest: '<%= path.fonts %>'
            },
            index: {
                expand: true,
                cwd: 'source/boffice/',
                src: 'index.html',
                dest: 'dist/boffice/'
            }
        },
        //Compass Task
        compass: {
            compile: {
                options: {
                    sassDir: '<%= path.sass %>',
                    cssDir: '<%= path.css %>',
                    imageDir: '<%= path.imgsrc %>',
                    environment: 'development',
                    outputStyle: 'compact',
                    noLineComments: true,
                    relativeAssets: true
                }
            }
        },
        //Imagemin Task
        imagemin: {
            allimg: {
                files: [{
                    expand: true,
                    cwd: '<%= path.imgsrc %>',
                    src: ['**/*.{png,jpg}'],
                    dest: '<%= path.img %>'
                }]
            }
        },
        //Htmlmin Task
        htmlmin: {
            options: {
                force: true,
                removeScriptTypeAttributes: true,
                removeComments: true,
                collapseWhitespace: false
            },
            allhtml: {
                files: [{
                    expand: true,
                    cwd: '<%= path.appsrc %>',
                    src: '**/**/*.html',
                    dest: '<%= path.app %>'
                }]
            }
        },
        //uglify
        uglify: {
            options: {
                mangle: false,
                compress: true
            },
            myjs: {
                files: [{
                    expand: true,
                    cwd: '<%= path.appsrc %>',
                    src: '**/**/*.js',
                    dest: '<%= path.app %>'
                }]
            }
        }
    });

    function runAllTasks() {
        grunt.task.run('clean:build');
        grunt.task.run('copy:alljs');
        grunt.task.run('copy:allcss');
        grunt.task.run('copy:allfonts');
        grunt.task.run('copy:index');
        grunt.task.run('uglify:myjs');
        grunt.task.run('imagemin:allimg');
        grunt.task.run('compass:compile');
        grunt.task.run('htmlmin:allhtml');
    }


    // Plugins.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Default task(s).
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', runAllTasks);
};
