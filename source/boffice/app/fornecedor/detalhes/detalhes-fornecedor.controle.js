define(['angularAMD'], function(app) {
	app.controller('DetalhesFornecedorControle', DetalhesFornecedorControle);

	DetalhesFornecedorControle.$inject = ['$scope', '$uibModalInstance'];

	function DetalhesFornecedorControle($scope, $uibModalInstance) {

		$scope.fechar = function() {
			$uibModalInstance.dismiss('cancel');
		};

	}

});