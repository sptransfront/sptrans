//TODO: Verificar porque angularAMD deve ser o primeiro item do array, e nome do modulo.
define(['angularAMD'], function(app) {
	app.controller('IncluirFornecedorControle', IncluirFornecedorControle);

	IncluirFornecedorControle.$inject = ['$scope', '$uibModalInstance'];

	function IncluirFornecedorControle($scope, $uibModalInstance) {

		$scope.fechar = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}

});