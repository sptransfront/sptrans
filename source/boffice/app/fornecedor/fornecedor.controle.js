(function() {

	'use strict';

	define(['angularAMD', 'sptListaDiretiva', 'dirPagination', 'sptCabecalhoPaginaDiretiva', 'sptModalDiretiva', 'recursoServico', 'recursosFabrica'], function(app) {
		app.controller('FornecedorControle', ['$scope', '$rootScope', '$uibModal', 'recursoServico', 'RecursosFabrica',
			function($scope, $rootScope, $uibModal, recursoServico, RecursosFabrica) {

				$scope.items = recursoServico.listarDados();

				var empresas = RecursosFabrica.gera('empresas');

				empresas.get('', function(data) {
					console.log(data);
				})

				$scope.deletarDados = function(id) {
					$scope.items = recursoServico.deletarDados(null, id);
				}

			}
		]);
	});

})();