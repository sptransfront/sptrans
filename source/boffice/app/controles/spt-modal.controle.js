//TODO: Verificar porque angularAMD deve ser o primeiro item do array, e nome do modulo.
define(['angularAMD', 'app/fabricas/modal.fabrica.js'], function(app) {
	app.controller('sptModalControle', sptModalControle);

	sptModalControle.$inject = ['$scope', '$uibModal', 'ModalFabrica'];

	function sptModalControle($scope, $uibModal, ModalFabrica) {

		$scope.abrirModal = function(modal) {
			ModalFabrica.abrir(modal);
		}
	}
});