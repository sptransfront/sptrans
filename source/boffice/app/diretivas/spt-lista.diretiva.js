define(['angularAMD'], function (app) {

	app.directive('sptListaDiretiva', function () {

		return{
			restrict: 'E',
			scope: true,
			templateUrl: 'app/templates/spt-lista.diretiva.html',
			link: function(scope, elem, attrs){
				scope.checkClick = function(el){
					alert('Edicação do id: ' + el);
				}				
			}
		}

	});

});