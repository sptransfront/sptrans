define(['angularAMD'], function(app) {

	app.directive('sptCabecalhoPaginaDiretiva', function() {
		return {
			restrict: 'E',
			scope: {
				titulo: '@'
			},
			transclude: true,
			templateUrl: 'app/templates/spt-cabecalho-pagina.diretiva.html'
		}
	});
});