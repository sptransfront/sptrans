define(['angularAMD', 'app/controles/spt-modal.controle.js'], function(app) {

	app.directive('sptModalDiretiva', function() {
		return {
			restrict: 'A',
			scope: {
				modulo: '@',
				acao: '@'
			},
			controller: "sptModalControle",
			link: function(scope, element, attrs) {

				element.on("click", function() {
					var modal = {
						modulo: scope.modulo,
						acao: scope.acao,
					}
					scope.abrirModal(modal);
				})

			}
		}
	});

});