define(['angularAMD'], function (app) {

	app.factory('ModalFabrica', ModalFabrica);

	ModalFabrica.$inject = ['$uibModal'];

	function ModalFabrica($uibModal) {

		function formatar(string) {
			return string.charAt(0).toUpperCase() + string.slice(1);
		}

		function getNomeControllerPorParametro(modulo, acao) {
			return formatar(acao) + formatar(modulo) + "Controle"
		}

		return {
			abrir: function (modal) {

				var caminho = "app/" + modal.modulo + "/" + modal.acao;

				require([caminho + "/" + modal.acao + "-" + modal.modulo + ".controle.js"], function () {
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: caminho + "/" + modal.acao + "-" + modal.modulo + ".html",
						controller: getNomeControllerPorParametro(modal.modulo, modal.acao),
						backdrop: 'static',
						size: '',
						resolve: {
						/*	parcial: function () {
								return obj.parcial
							}*/
						}
					});

				});
			}

		}
	}
});
