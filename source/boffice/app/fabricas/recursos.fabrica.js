/**
* RecursosFactory - Gera o recurso do sistema, baseado no ngResource
* @factory
* @param {string} strNomeRecurso - O nome do recurso na URL a ser acessado
* @param {[object]} objPropriedadesRecurso - As propriedades do recurso
*
* Acessar a doc em: https://docs.angularjs.org/api/ngResource/service/$resource
* Recurso sempre será baseado em
* http://URL_DO_RECURSO/NOME_RECURSO/:id
* id é o identificar único a ser trazido
* Caso id seja omitido, o padrão é trazer a lista completa de dados
*
* Exemplo de declaração do Recurso
* var usuario = RecursosFabrica.gera ( 'usuarios' , { id : '@id'} );
* 
*
* Exemplo de obtenção de todos os usuários:
* usuario.get ( null , function ( data ) { console.log ( data )  } );
*
* 
* Exemplo de criação:
* var usuario = new usuarios();
* usuario.nome = 'Maria';
* usuario.$save(); // Realiza um POST: /usuarios/ { nome : 'Maria' }
*
* 
* Exemplo de alteração/atualização:
* var usuario = usuarios.get ( { id : 1 } , function () { 
* 	usuario.nome = 'José';
* 	user.$save(); // Método para salvar/criar
* })
*
* 
* Exemplo de remoção:
*  usuario.$delete ( { id : 1 } , function () {
*  	alert ( 'Usuário removido com sucesso' );
*  })
*
* 
*/	

define ( [ 'angularAMD' , 'angular' , 'angular-resource' , 'configs' ] , function ( app ) {

	app.factory ( 'RecursosFabrica' , Recursos );

	function Recursos ( $resource ) {

		var url = getConfigs().paths.gateway;

		function Recurso ( strNomeRecurso , objPropriedadesRecurso ) {

			if ( !strNomeRecurso ) {
				throw Error ( 'RECURSOS' , 'Você deve informar um nome para o recurso' );
			}

			if ( !objPropriedadesRecurso ) {
				objPropriedadesRecurso = { id : '@id' };
			}

			return $resource (
				url + strNomeRecurso + '/:id'
				, objPropriedadesRecurso
			);
		}

		function gera ( strNomeRecurso , objPropriedadesRecurso ) {
			return new Recurso ( strNomeRecurso , objPropriedadesRecurso );
		}

		return {
			gera : gera
		};

	}

	return Recursos;
});