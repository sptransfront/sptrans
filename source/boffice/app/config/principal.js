require.config({

	baseUrl: '',

	paths: {
		'angular': 'app/angular',
		'angularAMD': 'app/terceiros/angularAMD',
		'ui-router': 'app/terceiros/angular-ui-router.min',
		'ui-bootstrap': 'app/terceiros/ui-bootstrap',
		'angular-sanitize': 'app/terceiros/angular-sanitize',
		'angular-animate': 'app/terceiros/angular-animate.min',
		'angular-locale_pt-br': 'app/terceiros/angular-locale_pt-br',
		'angular-resource': 'app/terceiros/angular-resource',
		'configs': 'app/config/configs',

		'sptListaDiretiva': 'app/diretivas/spt-lista.diretiva',
		'sptCabecalhoPaginaDiretiva': 'app/diretivas/spt-cabecalho-pagina.diretiva',
		'sptModalDiretiva': 'app/diretivas/spt-modal.diretiva',
		'dirPagination': 'app/terceiros/dirPagination',

		'recursosFabrica': 'app/fabricas/recursos.fabrica',

		'recursoServico': 'app/servicos/recurso.servico',

		'app': 'app/app.modulo',
	},

	shim: {
		'angularAMD': ['angular'],
		'ui-router': ['angular'],
		'angular-sanitize': ['angular'],
		'ui-bootstrap': ['angular'],
		'angular-animate': ['angular'],
		'angular-locale_pt-br': ['angular'],
		'angular-resource': ['angular']
	},

	deps: ['app']

});