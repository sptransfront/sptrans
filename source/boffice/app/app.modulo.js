define(['angularAMD', 'angular', 'ui-bootstrap', 'ui-router'], function (angularAMD) {

    var app = angular.module('app', [
        'ui.router', 'ui.bootstrap'
    ]);

    var getNomeControllerPorParametro = function(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1)+"Controle";
    }

    app.config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider)
        {
            $stateProvider
                .state('default', angularAMD.route({
                    url: '/:parametro',
                    templateUrl: function($stateParams)
                    {
                        return 'app/'+$stateParams.parametro+'/'+$stateParams.parametro+'.html';
                    },
                    resolve: {
                        loadController: ['$q', '$stateParams',
                            //execurado antes de chamar a rota
                            function ($q, $stateParams)
                            {
                                var parametro = $stateParams.parametro;
                                var deferred = $q.defer();
                                    require(['app/'+parametro+'/'+parametro+'.controle.js'], function () { deferred.resolve(); });
                                return deferred.promise;
                            }]
                    },
                    controllerProvider: function ($stateParams)
                    {
                        //chama apos a rota o resolve ser  realizado
                        return getNomeControllerPorParametro($stateParams.parametro);
                    }
                }));

            $urlRouterProvider.otherwise('/fornecedor');
        
        }
    ]);

    return angularAMD.bootstrap(app);
})

                