define(['angularAMD'], function (app) {

	app.service('recursoServico', function () {
		
		this.postarDados = function(url, obj){
			
		},

		this.listarDados = function(url, id){
			var data = {"fornecedores" :[			
			{ "id": 1, "identificador": 2321, "razao_social": "Empresa 1", "cnpj": "07.972.725/0001-90" },
			{ "id": 2, "identificador": 5634, "razao_social": "Empresa 2", "cnpj": "92.125.903/0001-17" },
			{ "id": 3, "identificador": 8987, "razao_social": "Empresa 3", "cnpj": "56.283.526/0001-80" },
			{ "id": 4, "identificador": 2321, "razao_social": "Empresa 4", "cnpj": "07.972.725/0001-90" },
			{ "id": 5, "identificador": 5634, "razao_social": "Empresa 5", "cnpj": "92.125.903/0001-17" },
			{ "id": 6, "identificador": 8987, "razao_social": "Empresa 6", "cnpj": "56.283.526/0001-80" },
			{ "id": 7, "identificador": 2321, "razao_social": "Empresa 7", "cnpj": "07.972.725/0001-90" },
			{ "id": 8, "identificador": 5634, "razao_social": "Empresa 8", "cnpj": "92.125.903/0001-17" },
			{ "id": 9, "identificador": 8987, "razao_social": "Empresa 9", "cnpj": "56.283.526/0001-80" },
			{ "id": 10, "identificador": 2321, "razao_social": "Empresa 10", "cnpj": "07.972.725/0001-90" },
			{ "id": 11, "identificador": 5634, "razao_social": "Empresa 11", "cnpj": "92.125.903/0001-17" },
			{ "id": 12, "identificador": 8987, "razao_social": "Empresa 12", "cnpj": "56.283.526/0001-80" },
			{ "id": 13, "identificador": 2321, "razao_social": "Empresa 13", "cnpj": "07.972.725/0001-90" },
			{ "id": 14, "identificador": 5634, "razao_social": "Empresa 14", "cnpj": "92.125.903/0001-17" },
			{ "id": 15, "identificador": 8987, "razao_social": "Empresa 15", "cnpj": "56.283.526/0001-80" },
			{ "id": 16, "identificador": 2321, "razao_social": "Empresa 16", "cnpj": "07.972.725/0001-90" },
			{ "id": 17, "identificador": 5634, "razao_social": "Empresa 17", "cnpj": "92.125.903/0001-17" },
			{ "id": 18, "identificador": 8987, "razao_social": "Empresa 18", "cnpj": "56.283.526/0001-80" },
			{ "id": 19, "identificador": 2321, "razao_social": "Empresa 19", "cnpj": "07.972.725/0001-90" },
			{ "id": 20, "identificador": 5634, "razao_social": "Empresa 20", "cnpj": "92.125.903/0001-17" },
			{ "id": 21, "identificador": 8987, "razao_social": "Empresa 21", "cnpj": "56.283.526/0001-80" },
			{ "id": 22, "identificador": 2321, "razao_social": "Empresa 22", "cnpj": "07.972.725/0001-90" },
			{ "id": 23, "identificador": 5634, "razao_social": "Empresa 23", "cnpj": "92.125.903/0001-17" },
			{ "id": 24, "identificador": 8987, "razao_social": "Empresa 24", "cnpj": "56.283.526/0001-80" },
			{ "id": 25, "identificador": 2321, "razao_social": "Empresa 25", "cnpj": "07.972.725/0001-90" },
			{ "id": 26, "identificador": 5634, "razao_social": "Empresa 26", "cnpj": "92.125.903/0001-17" },
			{ "id": 27, "identificador": 8987, "razao_social": "Empresa 27", "cnpj": "56.283.526/0001-80" },
			{ "id": 28, "identificador": 2321, "razao_social": "Empresa 28", "cnpj": "07.972.725/0001-90" },
			{ "id": 29, "identificador": 5634, "razao_social": "Empresa 29", "cnpj": "92.125.903/0001-17" },
			{ "id": 30, "identificador": 8987, "razao_social": "Empresa 30", "cnpj": "56.283.526/0001-80" },
			{ "id": 31, "identificador": 2321, "razao_social": "Empresa 31", "cnpj": "07.972.725/0001-90" },
			{ "id": 32, "identificador": 5634, "razao_social": "Empresa 32", "cnpj": "92.125.903/0001-17" },
			{ "id": 33, "identificador": 8987, "razao_social": "Empresa 33", "cnpj": "56.283.526/0001-80" },
			{ "id": 34, "identificador": 2321, "razao_social": "Empresa 34", "cnpj": "07.972.725/0001-90" },
			{ "id": 35, "identificador": 5634, "razao_social": "Empresa 35", "cnpj": "92.125.903/0001-17" },
			{ "id": 36, "identificador": 8987, "razao_social": "Empresa 36", "cnpj": "56.283.526/0001-80" }
			]};

			return data.fornecedores;

		},

		this.atualizarDados = function(url, id, obj){

		},

		this.deletarDados = function(url, id){
			
			var data = {"fornecedores" :[			
			{ "id": 1, "identificador": 2321, "razao_social": "Empresa 1", "cnpj": "07.972.725/0001-90" },
			{ "id": 2, "identificador": 5634, "razao_social": "Empresa 2", "cnpj": "92.125.903/0001-17" },
			{ "id": 3, "identificador": 8987, "razao_social": "Empresa 3", "cnpj": "56.283.526/0001-80" },
			{ "id": 4, "identificador": 2321, "razao_social": "Empresa 4", "cnpj": "07.972.725/0001-90" },
			{ "id": 5, "identificador": 5634, "razao_social": "Empresa 5", "cnpj": "92.125.903/0001-17" },
			{ "id": 6, "identificador": 8987, "razao_social": "Empresa 6", "cnpj": "56.283.526/0001-80" },
			{ "id": 7, "identificador": 2321, "razao_social": "Empresa 7", "cnpj": "07.972.725/0001-90" },
			{ "id": 8, "identificador": 5634, "razao_social": "Empresa 8", "cnpj": "92.125.903/0001-17" },
			{ "id": 9, "identificador": 8987, "razao_social": "Empresa 9", "cnpj": "56.283.526/0001-80" },
			{ "id": 10, "identificador": 2321, "razao_social": "Empresa 10", "cnpj": "07.972.725/0001-90" },
			{ "id": 11, "identificador": 5634, "razao_social": "Empresa 11", "cnpj": "92.125.903/0001-17" },
			{ "id": 12, "identificador": 8987, "razao_social": "Empresa 12", "cnpj": "56.283.526/0001-80" },
			{ "id": 13, "identificador": 2321, "razao_social": "Empresa 13", "cnpj": "07.972.725/0001-90" },
			{ "id": 14, "identificador": 5634, "razao_social": "Empresa 14", "cnpj": "92.125.903/0001-17" },
			{ "id": 15, "identificador": 8987, "razao_social": "Empresa 15", "cnpj": "56.283.526/0001-80" },
			{ "id": 16, "identificador": 2321, "razao_social": "Empresa 16", "cnpj": "07.972.725/0001-90" },
			{ "id": 17, "identificador": 5634, "razao_social": "Empresa 17", "cnpj": "92.125.903/0001-17" },
			{ "id": 18, "identificador": 8987, "razao_social": "Empresa 18", "cnpj": "56.283.526/0001-80" },
			{ "id": 19, "identificador": 2321, "razao_social": "Empresa 19", "cnpj": "07.972.725/0001-90" },
			{ "id": 20, "identificador": 5634, "razao_social": "Empresa 20", "cnpj": "92.125.903/0001-17" },
			{ "id": 21, "identificador": 8987, "razao_social": "Empresa 21", "cnpj": "56.283.526/0001-80" },
			{ "id": 22, "identificador": 2321, "razao_social": "Empresa 22", "cnpj": "07.972.725/0001-90" },
			{ "id": 23, "identificador": 5634, "razao_social": "Empresa 23", "cnpj": "92.125.903/0001-17" },
			{ "id": 24, "identificador": 8987, "razao_social": "Empresa 24", "cnpj": "56.283.526/0001-80" },
			{ "id": 25, "identificador": 2321, "razao_social": "Empresa 25", "cnpj": "07.972.725/0001-90" },
			{ "id": 26, "identificador": 5634, "razao_social": "Empresa 26", "cnpj": "92.125.903/0001-17" },
			{ "id": 27, "identificador": 8987, "razao_social": "Empresa 27", "cnpj": "56.283.526/0001-80" },
			{ "id": 28, "identificador": 2321, "razao_social": "Empresa 28", "cnpj": "07.972.725/0001-90" },
			{ "id": 29, "identificador": 5634, "razao_social": "Empresa 29", "cnpj": "92.125.903/0001-17" },
			{ "id": 30, "identificador": 8987, "razao_social": "Empresa 30", "cnpj": "56.283.526/0001-80" },
			{ "id": 31, "identificador": 2321, "razao_social": "Empresa 31", "cnpj": "07.972.725/0001-90" },
			{ "id": 32, "identificador": 5634, "razao_social": "Empresa 32", "cnpj": "92.125.903/0001-17" },
			{ "id": 33, "identificador": 8987, "razao_social": "Empresa 33", "cnpj": "56.283.526/0001-80" },
			{ "id": 34, "identificador": 2321, "razao_social": "Empresa 34", "cnpj": "07.972.725/0001-90" },
			{ "id": 35, "identificador": 5634, "razao_social": "Empresa 35", "cnpj": "92.125.903/0001-17" },
			{ "id": 36, "identificador": 8987, "razao_social": "Empresa 36", "cnpj": "56.283.526/0001-80" }
			]};

			id += - 1;

			delete data.fornecedores[id];

			var fornecedores = data.fornecedores.filter(function(){return true;});
			return fornecedores;
		}
	});

});